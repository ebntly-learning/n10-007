# Summary

* [1.0 Networking Concepts](./1/README.md)
    * [1.1](./1/1/README.md)
        * [Protocols and Ports](./1/1/protocols-and-ports.md)
        * [Protocol Types](./1/1/protocol-types.md)
        * [Connection Oriented vs. connectionless](./1/1/connection-oriented-vs-connectionless.md)
    * [1.2](./1/2/README.md)
        * [Layer 1 - Physical](./1/2/layer-1-physical.md)
        * [Layer 2 - Data link](./1/2/layer-2-data-link.md)
        * [Layer 3 - Network](./1/2/layer-3-network.md)
        * [Layer 4 - Transport](./1/2/layer-4-transport.md)
        * [Layer 5 - Session](./1/2/layer-5-session.md)
        * [Layer 6 - Presentation](./1/2/layer-6-presentation.md)
        * [Layer 7 - Application](./1/2/layer-7-application.md)
    
    

